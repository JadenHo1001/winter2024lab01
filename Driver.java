import java.util.Scanner;

public class Driver {

    public static void main(String[] args) {
        System.out.println("Welcome to the Calculator App!");

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the first number to add: ");
        int num1 = scanner.nextInt();
        System.out.print("Enter the second number to add: ");
        int num2 = scanner.nextInt();

        int sum = Calculator.add(num1, num2);
        System.out.println("The sum of " + num1 + " and " + num2 + " is: " + sum);

        System.out.print("Enter a number to calculate its square root: ");
        int numberForSquareRoot = scanner.nextInt();

        double squareRootResult = Calculator.squareRoot(numberForSquareRoot);
        System.out.println("The square root of " + numberForSquareRoot + " is: " + squareRootResult);

        int randomNumber = Calculator.randomNumber();
        System.out.println("Here is a random number: " + randomNumber);

        scanner.close();
    }
}
