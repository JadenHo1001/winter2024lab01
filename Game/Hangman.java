import java.util.Scanner;

public class Hangman {
    // Function to check if a letter is in the word and return its index if found, -1 otherwise
    public static int isLetterInWord(String word, char c) {
        for (int i = 0; i < 4; i++) {
            if (word.charAt(i) == c) {
                return i;
            }
        }
        return -1;
    }

    // Function to convert a character to uppercase
    public static char toUpperCase(char c) {
        return Character.toUpperCase(c);
    }

    // Function to print the current state of the word, revealing guessed letters and hiding others
    public static void printWork(String word, boolean[] letters) {
		//creates an empty string that updates when the input guess is wrong or correct
        String newWord = "";
		//checks the word to see if the input guess is inside the word
        for (int i = 0; i < 4; i++) {
            if (letters[i]) {
				//updates the empty string with the guessed char at the position its supposed to be in the word
                newWord += word.charAt(i);
            } else {
				//updates the empty string to indicate that the guessed char isn't in the word
                newWord += " _ ";
            }
        }
        System.out.println("Your result is " + newWord);
    }

    // Function to run the hangman game
    public static void runGame(String word) {
        boolean[] letters = new boolean[4]; // Array to track guessed letters
        int numberOfGuess = 0; // Counter for the number of incorrect guesses

        // Main game loop
        while (numberOfGuess < 6 && !(letters[0] && letters[1] && letters[2] && letters[3])) {
            Scanner reader = new Scanner(System.in);

            System.out.println("Guess a letter");
            char guess = reader.nextLine().charAt(0);
            guess = toUpperCase(guess); // Convert guess to uppercase

            int letterIndex = isLetterInWord(word, guess);
            if (letterIndex >= 0) {
                letters[letterIndex] = true; // Mark the guessed letter as found
            } else {
                numberOfGuess++; // Increment the count of incorrect guesses
            }

            printWork(word, letters); // Display the current state of the word
        }

        // Display game result
        if (numberOfGuess == 6) {
            System.out.println("Oops! Better luck next time :)");
        }

        if (letters[0] && letters[1] && letters[2] && letters[3]) {
            System.out.println("Congrats! You got it :)");
        }
    }
}
