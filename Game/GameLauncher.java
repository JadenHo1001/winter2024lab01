import java.util.Scanner;

public class GameLauncher{
	
	public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        System.out.println("Welcome to the Game Launcher :) ");
        System.out.println("Please enter HANGMAN if you wish to play a game of Hangman");
        System.out.println("Or alternatively, please enter the word WORDLE if you want to play Wordle");
		
        String game = reader.next().toUpperCase();
		//checks which game should be runned
		if(game.equals("HANGMAN")){
			System.out.println("Starting a game of Hangman...");

			System.out.println("Enter a 4-letter word:");
			String word = reader.next();
			word = word.toUpperCase(); // Convert the word to uppercase
			Hangman.runGame(word); // Start the hangman game
		}
		else if(game.equals("WORDLE")){
			System.out.println("Starting a game of Wordle...");
			
			System.out.println("Welcome to Wordle!");
			System.out.println("Guess a 5-letter word with no repeating characters");
			// Generate a random word for the user to guess
			String wordToGuess = Wordle.generateWord();
			// Start the game with the generated word
			Wordle.runGame(wordToGuess);
			// Close the scanner to prevent resource leaks
			reader.close();
		}
		System.out.println("Please have a nice day");
    }
}