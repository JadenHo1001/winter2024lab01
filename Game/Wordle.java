import java.util.Scanner;
import java.util.Random;

public class Wordle {

  // Method to generate a random word from a predefined list
  public static String generateWord() {
    Random random = new Random();
    String[] words = { "DEATH", "FLAME", "FINAL", "MIGHT", "HELIX", "HYPER", "GRIPS", "SWORD", "SNAKE", "DEMON",
        "WORLD", "RULER", "LIVES", "PAINT", "ANGEL", "OATHS", "SPOKE", "JUDGE", "VALOR", "FATED", "MOUNT", "FORMS",
        "ARSON", "GODLY", "CHASE", "MOTHS", "CHAOS", "BLADE", "BEAST", "UNITY", "RUBIA", "BEGIN", "STARK", "STARE",
        "CYBER", "PRIDE", "NIGHT", "PRIMA", "AEONS", "SIBYL", "CAUSE", "CRASH", "DRAFT", "FROST", "REACT", "STONE",
        "POWER", "TWINE", "BIRTH", "HUMAN", "LEAKS", "BLANK", "WATER", "BUILD", "QUASI", "BLACK", "TWINS", "LINKS",
        "LANCE", "RULES", "MONEY", "BODHI", "SWIFT", "LIGHT", "FANCY" };
    int randWordIndex = random.nextInt(words.length);
    return words[randWordIndex];
  }

  // Check if a given letter is in the word at a position other than the current
  // position
  // Check if a given letter is in the word at any position
public static boolean letterInWord(String word, char letter) {
    for (int i = 0; i < word.length(); i++) {
        if (word.charAt(i) == letter) {
            return true;
        }
    }
    return false;
}


  // Check if a given letter is in the word at a specific position
  public static boolean letterInSlot(String word, char letter, int position) {
    if (position >= 0 && position < word.length()) {
      return word.charAt(position) == letter;
    }
    return false;
  }

  // Evaluate the correctness of each letter in the guessed word
public static String[] guessWord(String answer, String guess) {
    int length = answer.length();
    String[] colors = new String[length];

    for (int i = 0; i < length; i++) {
        char guessedChar = guess.charAt(i);

        if (letterInSlot(answer, guessedChar, i)) {
            colors[i] = "green";
        } else if (letterInWord(answer, guessedChar)) {
            colors[i] = "yellow";
        } else {
            colors[i] = "white";
        }
    }

    return colors;
}


  // Display the current state of the guessed word with colored letters
  public static void presentResults(String word, String[] colors) {
    System.out.print("Current State: ");

    for (int i = 0; i < word.length(); i++) {
      char letter = word.charAt(i);
      String color = colors[i];

      switch (color) {
        case "green":
          System.out.print("\u001B[32m" + letter + "\u001B[0m");
          break;
        case "yellow":
          System.out.print("\u001B[33m" + letter + "\u001B[0m");
          break;
        case "white":
          System.out.print("\u001B[0m" + letter);
          break;
      }
    }

    System.out.println();
  }

  // Read the user's guess, ensuring it is a 5-letter word
  public static String readGuess() {
    String guess;
    Scanner scanner = new Scanner(System.in);

    do {
      System.out.println("Enter a guess word (5 letters): ");
      guess = scanner.nextLine().toUpperCase();
    } while (guess.length() != 5);

    return guess;
  }

  public static void runGame(String word) {
    Scanner scanner = new Scanner(System.in);

    int attempts = 0;
    final int maxAttempts = 6;
    boolean wordGuessed = false;
    // Continue the game until the user guesses the word or runs out of attempts
    while (attempts < maxAttempts && !wordGuessed) {
      // Get the user's guess and evaluate the correctness of each letter
      String guess = readGuess();
      String[] result = guessWord(word, guess);
      // Display the results of the guess
      presentResults(guess, result);
      // Check if the user guessed the word correctly
      if (word.equals(guess)) {
        wordGuessed = true;
        System.out.println("Congratulations! You guessed the word. You win!");
      } else {
        attempts++;
        System.out.println("Attempts left: " + (maxAttempts - attempts));
      }
    }
    // Display a message if the user runs out of attempts
    if (!wordGuessed) {
      System.out.println("Sorry, you're out of attempts. Try again. The correct word was: " + word);
    }
    // Close the scanner to prevent resource leaks
    scanner.close();
  }

}
