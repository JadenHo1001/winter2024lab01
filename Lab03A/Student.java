import java.util.Random;

public class Student {

    public String name;
    public String program;
    public int age;
    public int currentSemester;

    public void introduce() {
        System.out.println("Hello, my name is " + name + ". I'm " + age + " years old, and I study " + program + ".");
    }

    public void generateSemester() {
        Random random = new Random();
        currentSemester = random.nextInt(8) + 1;
        System.out.println(name + " is currently in semester " + currentSemester + ".");
    }
}
