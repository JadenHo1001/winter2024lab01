import java.util.Scanner;

public class Application{
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		
        Student[] section3 = new Student[3];
		Student caden = new Student();
		Student kayla = new Student();
        section3[2] = new Student();
        
        caden.name = "Caden";
        caden.program = "Psychology";
        caden.age = 18;
        
        kayla.name = "Kayla";
		kayla.program = "Economics";
        kayla.age = 22;
        
		section3[0] = caden;
		section3[1] = kayla;
        section3[2].name = "Jaden";
        section3[2].program = "Computer Science";
        section3[2].age = 18;

          for (Student student : section3) {
            student.introduce();
            student.generateSemester();
        }
    }
}