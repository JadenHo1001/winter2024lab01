import java.util.Random;

public class Calculator {
  public static int add(int num1, int num2) {
    return num1 + num2;
  }

  public static double squareRoot(int num) {
    return Math.sqrt(num);
  }

  public static double randomNumber() {
    Random rand = new Random();
    return rand.nextDouble();
  }

  public static double divide(int num1, int num2) {
    return (double) num1 / num2;
  }
}
